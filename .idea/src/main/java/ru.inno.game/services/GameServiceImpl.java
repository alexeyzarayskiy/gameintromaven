package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repositories.GamesRepository;
import ru.inno.game.repositories.PlayersRepository;
import ru.inno.game.repositories.ShotsRepository;

import java.time.Duration;
import java.time.LocalDateTime;

public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили информацию об обоих игроках
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        // создали игру
        Game game = Game.builder()
                .dateTime(LocalDateTime.now())
                .playerFirst(first)
                .playerSecond(second)
                .playerFirstShotsCount(0)
                .playerSecondShotsCount(0)
                .secondsGameTimeAmount(0L)
                .build();
        // сохранили игру в репозитории
        gamesRepository.safe(game);
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет первого игрока под таким именем
        if (player == null) {
            // создаем игрока
            player = Player.builder()
                    .name(nickname)
                    .ip(ip)
                    .build();
            // сохраняем его в реепозитории
            playersRepository.save(player);
        } else {
            // если такой игрок был - обновляем у него IP-адрес
            player.setIp(ip);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем того, кто стрелял, из репозитория
        Player shooter = playersRepository.findByNickname(shooterNickname);
        // получаем того, в кого стреляли, из репозитория
        Player target = playersRepository.findByNickname(targetNickname);
        // получаем игру
        Game game = gamesRepository.findById(gameId);
        // создаем выстрел
        Shot shot = Shot.builder()
                .dateTime(LocalDateTime.now())
                .game(game)
                .shooter(shooter)
                .target(target)
                .build();
        // увеличиваем очки у стреляющего
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелявший - первый игрок
        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        // если стреляаший - второй игрок
        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        //обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.safe(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId) {
        Game game = gamesRepository.findById(gameId);
        Player player1 = playersRepository.findByNickname(game.getPlayerFirst().getName());
        Player player2 = playersRepository.findByNickname(game.getPlayerSecond().getName());
        String winnerNickname;

        long dateTime = Duration.between(game.getDateTime(), LocalDateTime.now()).getSeconds();
        if (player1.getPoints() > player2.getPoints()) {
            winnerNickname = player1.getName();
            player1.setMaxWinsCount(player1.getMaxWinsCount() + 1);
            player2.setMaxLosesCount(player2.getMaxLosesCount() + 1);

        } else {
            winnerNickname = player2.getName();
            player1.setMaxLosesCount(player1.getMaxLosesCount() + 1);
            player2.setMaxWinsCount(player2.getMaxWinsCount() + 1);
        }


        playersRepository.update(player1);
        playersRepository.update(player2);
        gamesRepository.update(game);

        StatisticDto statisticDto = new StatisticDto(gameId, player1.getName(), player1.getPoints(), player1.getMaxWinsCount(),
                player2.getName(), player2.getPoints(), player2.getMaxWinsCount(), winnerNickname, dateTime);

        return statisticDto;
    }
}
