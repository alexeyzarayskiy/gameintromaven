package ru.inno.game.repositories;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;

public class GamesRepositoryJdbcImpl implements GamesRepository {
    // запросы, которые посылаем в базу данных
    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from game where id = ?";

    //language=SQL
    private static final String SQL_INSERT_GAME = "insert into game (datetime, player_first, player_second) " +
            " values (?, ?, ?);";

    //language=SQL
    private static final String SQL_UPDATE_GAME = "update game set (player_first_shots_count, player_second_shots_count, seconds_game_time_amount) = (?, ?, ?) where id = ?";

    // анонимный класс, реализованный через lambda-выражение, который позволяет преобразовать
    // строку из ResultSet в объект Game
    private static final RowMapper<Game> gameRowMapper = row -> Game.builder()
            .id(row.getLong("id"))
            .dateTime(LocalDateTime.parse(row.getString("datetime")))
            .playerFirstShotsCount(row.getInt("player_first_shots_count"))
            .playerSecondShotsCount(row.getInt("player_second_shots_count"))
            .playerFirst(Player.builder()
                    .name(row.getString("player_first"))
                    .build())
            .playerSecond(Player.builder()
                    .name(row.getString("player_second"))
                    .build())
            .secondsGameTimeAmount(row.getLong("seconds_game_time_amount"))
            .build();

    // зависимость
    private DataSource dataSource;

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public void safe(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, game.getDateTime().toString());
            statement.setString(2, game.getPlayerFirst().getName());
            statement.setString(3, game.getPlayerSecond().getName());
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can not insert");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()){
                if (generatedKeys.next()) {
                    Long id = generatedKeys.getLong("id");
                    game.setId(id);
                } else {
                    throw new SQLException(("Can not retrieve id"));
                }
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Game findById(Long gameId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {
            statement.setLong(1, gameId);
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return gameRowMapper.mapRow(rows);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME)) {
            statement.setLong(1, game.getSecondsGameTimeAmount());
            statement.setLong(2, game.getPlayerFirstShotsCount());
            statement.setLong(3, game.getPlayerSecondShotsCount());
            statement.setLong(4, game.getId());
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can not update");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
