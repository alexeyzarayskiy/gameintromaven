package ru.inno.game.repositories;

import ru.inno.game.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

public class ShotsRepositoryJdbcImpl implements ShotsRepository {

    //language=SQL
    private final static String SQL_INSERT_SHOT = "insert into shot (datetime, game, shooter, target) values (?, ?, ?, ?);";

    private DataSource dataSource;

    public ShotsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public void safe(Shot shot) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SHOT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, shot.getDateTime().toString());
            statement.setLong(2, shot.getGame().getId());
            statement.setString(3, shot.getShooter().getName());
            statement.setString(4, shot.getTarget().getName());
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can not insert");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    Long id = generatedKeys.getLong("id");
                    shot.setId(id);
                } else {
                    throw new SQLException(("Can not retrieve id"));
                }
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
