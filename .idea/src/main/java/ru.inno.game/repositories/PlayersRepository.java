package ru.inno.game.repositories;

import ru.inno.game.models.Player;

public interface PlayersRepository {
    Player findByNickname(String nickname);

    void save(Player player);

    void update(Player player);

    Player findById(Long id);
}
