package ru.inno.game.repositories;

import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;

public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    //language=SQL
    private final static String SQL_FIND_PLAYER_BY_NICKNAME = "select id, ip, name, points, max_wins_count, max_loses_count from player where name = ?";
    //language=SQL
    private final static String SQL_FIND_PLAYER_BY_ID = "select * from player where id = ?";
    //language=SQL
    private final static String SQL_INSERT_PLAYER = "insert into player (name, ip) values (?, ?);";
    //language=SQL
    private final static String SQL_UPDATE_PLAYER = "update player set (name, ip, points, max_wins_count, max_loses_count) = (?, ?, ?, ?, ?) where id = ?";

    private DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Player> playerRowMapper = row -> Player.builder()
            .id(row.getLong("id"))
            .ip(row.getString("ip"))
            .name(row.getString("name"))
            .points(row.getInt("points"))
            .maxWinsCount(row.getInt("max_wins_count"))
            .maxLosesCount(row.getInt("max_loses_count"))
            .build();

    @Override
    public Player findByNickname(String nickname) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_NICKNAME)) {
            statement.setString(1, nickname);
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return playerRowMapper.mapRow(rows);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             // JDBC возвращает созданный id
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getName());
            statement.setString(2, player.getIp());
            // affectedRows - сколько строк было изменено в базе данных
            // в случае однократного insert это число должно равняться одному
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can not insert");
            }
            // запросили сгенерированные базой данных ключи
            try (ResultSet generatedKeys = statement.getGeneratedKeys()){
                // если там что-то есть, давайте это возьмем
                if (generatedKeys.next()) {
                    // попросили сгенерированный БД id
                    Long id = generatedKeys.getLong("id");
                    player.setId(id);
                } else {
                    // если ключи не вернулись
                    throw new SQLException(("Can not retrieve id"));
                }
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PLAYER)) {
            statement.setString(1, player.getName());
            statement.setString(2, player.getIp());
            statement.setLong(3, player.getPoints());
            statement.setLong(4, player.getMaxWinsCount());
            statement.setLong(5, player.getMaxLosesCount());
            statement.setLong(6, player.getId());
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can not update");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Player findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_ID)) {
            statement.setLong(1, id);
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return playerRowMapper.mapRow(rows);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
