package ru.inno.game.repositories;

import ru.inno.game.models.Game;

public interface GamesRepository {
    void safe(Game game);

    Game findById(Long gameId);

    void update(Game game);
}
