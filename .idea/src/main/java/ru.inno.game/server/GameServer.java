package ru.inno.game.server;

import ru.inno.game.services.GameService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

// отвечает за подключение игроков по протоколу socket
public class GameServer {

    private ClientThread firstPlayer;
    private ClientThread secondPlayer;
    private ServerSocket serverSocket;
    // флаг, который определяет, началась ли игра
    private boolean isGameStarted = false;
    // время начала игры 
    private long startTimeMills;
    private boolean isGameInProcess = true;
    // идентификатор игры
    private long gameId;
    // объект бизнес-логики игры
    private GameService gameService;
    // мьютекс - объект синхронизации
    private Lock lock = new ReentrantLock();

    public GameServer(GameService gameService) {
        this.gameService = gameService;
    }

    // метод запуска сервера на определенном порту
    public void start(int port) {
        try {
            // запсутили SocketServer на определенном порту
            serverSocket = new ServerSocket(port);
            System.out.println("СЕРВЕР ЗАПУЩЕН...");
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ПЕРВОГО КЛИЕНТА...");
            firstPlayer = connect();
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ВТОРОГО КЛИЕНТА...");
            secondPlayer = connect();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private ClientThread connect() {
        Socket client;
        // получили клиента
        try {
            // уводит приложение в ожидание (wait), пока не присоединится какой-либо клиент
            // как только клиент подключен к сверверу,
            // объект-соединение возвращается как результат выполнения метода
            client = serverSocket.accept();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        // создали ему отдельный поток
        ClientThread clientThread = new ClientThread(client);
        // запустили отдельный бесконечный поток
        clientThread.start();
        //отправили клиенту сообщение о подключении
        System.out.println("КЛИЕНТ ПОДКЛЮЧЕН...");
        clientThread.sendMessage("Вы подключены к серверу");
        return clientThread;
    }

    // отдельный поток для клиента
    private class ClientThread extends Thread {

        private final PrintWriter toClient;
        private final BufferedReader fromClient;
        // имя игрока, который находится в текущем потоке
        private String playerNickname;
        private String ip;

        public ClientThread(Socket client) {
            try {
                // задача конструктора - получить потоки чтения/записи
                // autoFlush - чтобы сразу отправлял данные в поток

                // оборачиваем байтовые потоки в символьные потоки
                this.toClient = new PrintWriter(client.getOutputStream(), true);
                this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
                this.ip = client.getInetAddress().getHostAddress();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        // метод, который работает в отдельном потоке на протяжении всей программы
        @Override
        public void run() {
            // бесконечно работающий клиентский поток
            // ожидает сообщение от клиента
            while (isGameInProcess) {
                String messageFromClient;
                try {
                    messageFromClient = fromClient.readLine();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
                if (messageFromClient != null) {
                    if (isMessageForNickname(messageFromClient)) {
                        resolveNickname(messageFromClient);
                    } else if (isMessageForExit(messageFromClient) && isGameInProcess) {
                        // только один поток может завершить в конкретный момент времени игру
                        lock.lock();
                        gameService.finishGame(gameId);
                        isGameInProcess = false;
                        lock.unlock();
                    }
                }
                // если первый игрок задал себе имя и второй игрок задал себе имя
                // и при этом игра еще не началась

                // делаем проверку только одним потоком
                lock.lock();
                if (isReadyForStartGame()) {
                    // вызываем метод бизнес-логики для начала игры
                    gameId = gameService.startGame(firstPlayer.getIp(), secondPlayer.getIp(), firstPlayer.playerNickname, secondPlayer.playerNickname);
                    // устанавливаем флаг начала игры
                    startTimeMills = System.currentTimeMillis();
                    isGameStarted = true;
                }
                lock.unlock();
            }
        }

        private boolean isMessageForExit(String messageFromClient) {
            return messageFromClient.equals("exit");
        }

        private boolean isMessageForNickname(String messageFromClient) {
            return messageFromClient.startsWith("name: ");
        }

        private boolean isReadyForStartGame() {
            return firstPlayer.playerNickname != null && secondPlayer.playerNickname != null && !isGameStarted;
        }

        private void resolveNickname(String messageFromClient) {
            if (meFirst()) {
                fixNickname(messageFromClient, firstPlayer, "ОТ ПЕРВОГО ИГРОКА: ", secondPlayer);
            } else {
                fixNickname(messageFromClient, secondPlayer, "ОТ ВТОРОГО ИГРОКА: ", firstPlayer);
            }
        }

        private void fixNickname(String nickname, ClientThread currentPlayer, String anotherMessagePrefix, ClientThread anotherPlayer) {
            currentPlayer.playerNickname = nickname.substring(6);
            System.out.println(anotherMessagePrefix + nickname);
            anotherPlayer.sendMessage(nickname);
        }

        public void sendMessage(String message) {
            toClient.println(message);
        }

        private boolean meFirst() {
            return this == firstPlayer;
        }

        public String getIp() {
            return ip;
        }
    }
}
