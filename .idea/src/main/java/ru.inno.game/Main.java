package ru.inno.game;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.game.dto.StatisticDto;
import ru.inno.game.repositories.*;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;


import javax.sql.DataSource;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl("jdbc:postgresql://localhost:5432/game_db");
        configuration.setDriverClassName("org.postgresql.Driver");
        configuration.setUsername("postgres");
        configuration.setPassword("Kd8sSeld");
        configuration.setMaximumPoolSize(20);
        DataSource dataSource = new HikariDataSource(configuration);

        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        System.out.println("Введите имена игроков");

        Scanner scanner = new Scanner(System.in);

        String first = scanner.nextLine();
        String second = scanner.nextLine();
        Random random = new Random();

        int j = 0;
        while (j < 3) {
            j++;

            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
            String shooter = first;
            String target = second;
            int i = 0;

            while (i < 4) {
                System.out.println(shooter + " делайте выстрел в " + target);
                scanner.nextLine();

                int success = random.nextInt(2);

                if (success == 0) {
                    System.out.println("Успешно!");
                    gameService.shot(gameId, shooter, target);
                } else {
                    System.out.println("Промах!");
                }

                String temp = shooter;
                shooter = target;
                target = temp;
                i++;
            }
            StatisticDto statistic = gameService.finishGame(gameId);
            System.out.println(statistic);
        }
    }
}
