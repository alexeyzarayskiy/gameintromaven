package ru.inno.game;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.game.repositories.GamesRepository;
import ru.inno.game.repositories.GamesRepositoryJdbcImpl;
import ru.inno.game.repositories.PlayersRepository;
import ru.inno.game.repositories.PlayersRepositoryJdbcImpl;
import ru.inno.game.server.GameServer;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;

import javax.sql.DataSource;

public class MainServer {
    public static void main(String[] args) {
        // ConnectionPool - пул соединений с базой данных
        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl("jdbc:postgresql://localhost:5432/game_db");
        configuration.setDriverClassName("org.postgresql.Driver");
        configuration.setUsername("postgres");
        configuration.setPassword("Kd8sSeld");
        configuration.setMaximumPoolSize(20);
        DataSource dataSource = new HikariDataSource(configuration);
        // создаем репозиторий, который использует данный источник данных
        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        // создали сервис, который использует созданные выше репозитории
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, null);
        // передали сервис объекту-серверу для игры
        GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);
    }
}
