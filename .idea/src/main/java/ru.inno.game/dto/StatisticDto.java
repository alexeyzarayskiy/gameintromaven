package ru.inno.game.dto;


import java.util.StringJoiner;

// информация об игре, dto - DataTransferObject
public class StatisticDto {
    long id;
    String player1;
    int player1Points;
    int player1TotalPoints;
    String player2;
    int player2Points;
    int player2TotalPoints;
    String winner;
    long dateTime;

    public StatisticDto(long id, String player1, int player1Points, int player1TotalPoints,
                        String player2, int player2Points, int player2TotalPoints, String winner, long dateTime) {
        this.id = id;
        this.player1 = player1;
        this.player1Points = player1Points;
        this.player1TotalPoints = player1TotalPoints;
        this.player2 = player2;
        this.player2Points = player2Points;
        this.player2TotalPoints = player2TotalPoints;
        this.winner = winner;
        this.dateTime = dateTime;
    }


    @Override
    public String toString() {
        return new StringJoiner("")
                .add("Игра с ID = " + id + "\n")
                .add("Игрок 1: " + player1 + ", попаданий - " + player1Points + ", всего очков - " + player1TotalPoints + "\n")
                .add("Игрок 2: " + player2 + ", попаданий - " + player2Points + ", всего очков - " + player2TotalPoints + "\n")
                .add("Победа: " + winner + "\n")
                .add("Игра длилась: " + dateTime + " секунд" + "\n")
                .toString();
    }
}
